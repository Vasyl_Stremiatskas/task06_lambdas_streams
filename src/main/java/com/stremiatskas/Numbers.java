package com.stremiatskas;

public interface Numbers<T>{
    int  execute(T a, T b);
}